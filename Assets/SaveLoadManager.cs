﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SaveLoadManager : MonoBehaviour
{
    public static SaveLoadManager SaveLoadManagerInstance;
    [SerializeField] NavMeshAgent shepherd;
    [SerializeField] NavMeshAgent dog;
    [SerializeField] NavMeshAgent sheep0;
    [SerializeField] NavMeshAgent sheep1;
    [SerializeField] NavMeshAgent sheep2;
    [SerializeField] NavMeshAgent sheep3;
    [SerializeField] NavMeshAgent sheep4;
    [SerializeField] NavMeshAgent sheep5;
    [SerializeField] NavMeshAgent sheep6;
    [SerializeField] NavMeshAgent sheep7;
    [SerializeField] NavMeshAgent sheep8;
    [SerializeField] NavMeshAgent sheep9;
    [SerializeField] NavMeshAgent sheep10;
    [SerializeField] NavMeshAgent sheep11;
    [SerializeField] NavMeshAgent sheep12;

    [SerializeField] SheepManager sheeps;
    [SerializeField] AreaManager areaManager;

    
    
    void Start()
    {
        SaveLoadManagerInstance = this;
        Load();
    }

    void Load()
    {
        int waypoint = PlayerPrefs.GetInt("Waypoint");

        switch (waypoint)
        {
            case 0:
                return;
                break;
            case 1:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 1;
                areaManager._currentIndex = 0;
                shepherd.Warp(new Vector3(12, shepherd.nextPosition.y, -42));
                dog.Warp(new Vector3(45, dog.nextPosition.y, -40));
                sheep0.Warp(new Vector3(28, sheep0.nextPosition.y, -38));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                break;
            case 2:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 3;
                areaManager._currentIndex = 1;
                shepherd.Warp(new Vector3(66, shepherd.nextPosition.y,-60));
                dog.Warp(new Vector3(87, dog.nextPosition.y, -67));
                sheep0.Warp(new Vector3(89, sheep0.nextPosition.y, -82));
                sheep1.Warp(new Vector3(104, sheep1.nextPosition.y, -42));
                sheep2.Warp(new Vector3(118, sheep2.nextPosition.y, -69));                
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());   
                break;
            case 3:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 5;
                areaManager._currentIndex = 2;
                shepherd.Warp(new Vector3(105, shepherd.nextPosition.y,-97));
                dog.Warp(new Vector3(110, dog.nextPosition.y, -105));
                sheep0.Warp(new Vector3(105, sheep0.nextPosition.y, -154));
                sheep1.Warp(new Vector3(109, sheep1.nextPosition.y, -140));
                sheep2.Warp(new Vector3(81, sheep2.nextPosition.y, -137));
                sheep3.Warp(new Vector3(100, sheep3.nextPosition.y, -110));
                sheep4.Warp(new Vector3(120, sheep4.nextPosition.y, -112));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep5.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep6.gameObject.GetComponent<SheepBehaviour>());
                break;
            case 4:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 7;
                areaManager._currentIndex = 3;
                shepherd.Warp(new Vector3(72, shepherd.nextPosition.y, -120));
                dog.Warp(new Vector3(75, dog.nextPosition.y, -110));
                sheep0.Warp(new Vector3(66, sheep0.nextPosition.y, -98));
                sheep1.Warp(new Vector3(64, sheep1.nextPosition.y, -87));
                sheep2.Warp(new Vector3(53, sheep2.nextPosition.y, -79));
                sheep3.Warp(new Vector3(46, sheep3.nextPosition.y, -102));
                sheep4.Warp(new Vector3(52, sheep4.nextPosition.y, -116));
                sheep5.Warp(new Vector3(49, sheep5.nextPosition.y, -131));
                sheep6.Warp(new Vector3(31, sheep6.nextPosition.y, -120));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep5.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep6.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep7.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep8.gameObject.GetComponent<SheepBehaviour>());
                break;
            case 5:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 9;
                areaManager._currentIndex = 4;
                shepherd.Warp(new Vector3(16, shepherd.nextPosition.y, -130));
                dog.Warp(new Vector3(0, dog.nextPosition.y, -136));
                sheep0.Warp(new Vector3(-3, sheep0.nextPosition.y, -150));
                sheep1.Warp(new Vector3(-13, sheep1.nextPosition.y, -162));
                sheep2.Warp(new Vector3(-33, sheep2.nextPosition.y, -142));
                sheep3.Warp(new Vector3(-26, sheep3.nextPosition.y, -131));
                sheep4.Warp(new Vector3(-12, sheep4.nextPosition.y, -166));
                sheep5.Warp(new Vector3(-11, sheep5.nextPosition.y, -155));
                sheep6.Warp(new Vector3(7, sheep6.nextPosition.y, -141));
                sheep7.Warp(new Vector3(4, sheep7.nextPosition.y, -127));
                sheep8.Warp(new Vector3(-10, sheep8.nextPosition.y, -123));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep5.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep6.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep7.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep8.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep9.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep10.gameObject.GetComponent<SheepBehaviour>());
                break;
            case 6:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 11;
                areaManager._currentIndex = 5;
                shepherd.Warp(new Vector3(-28, shepherd.nextPosition.y, -110));
                dog.Warp(new Vector3(-24, dog.nextPosition.y, -80));
                sheep0.Warp(new Vector3(-30, sheep0.nextPosition.y, -85));
                sheep1.Warp(new Vector3(-22, sheep1.nextPosition.y, -97));
                sheep2.Warp(new Vector3(-33, sheep2.nextPosition.y, -102));
                sheep3.Warp(new Vector3(-35, sheep3.nextPosition.y, -95));
                sheep4.Warp(new Vector3(-30, sheep4.nextPosition.y, -88));
                sheep5.Warp(new Vector3(-45, sheep5.nextPosition.y, -86));
                sheep6.Warp(new Vector3(-51, sheep6.nextPosition.y, -79));
                sheep7.Warp(new Vector3(-39, sheep7.nextPosition.y, -70));
                sheep8.Warp(new Vector3(-26, sheep8.nextPosition.y, -60));
                sheep9.Warp(new Vector3(-7, sheep9.nextPosition.y, -57));
                sheep10.Warp(new Vector3(-10, sheep10.nextPosition.y, -78));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep5.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep6.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep7.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep8.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep9.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep10.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep11.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep12.gameObject.GetComponent<SheepBehaviour>());
                break;
            case 7:
                shepherd.GetComponent<ShepherdBehaviour>()._currentWaypoint = 13;
                areaManager._currentIndex = 6;
                shepherd.Warp(new Vector3(-15, shepherd.nextPosition.y, -40));
                dog.Warp(new Vector3(0, dog.nextPosition.y, -25));
                sheep0.Warp(new Vector3(-21, sheep0.nextPosition.y, -18));
                sheep1.Warp(new Vector3(-20, sheep1.nextPosition.y, -28));
                sheep2.Warp(new Vector3(-13, sheep2.nextPosition.y, -36));
                sheep3.Warp(new Vector3(0, sheep3.nextPosition.y, -40));
                sheep4.Warp(new Vector3(7, sheep4.nextPosition.y, -35));
                sheep5.Warp(new Vector3(7, sheep5.nextPosition.y, -25));
                sheep6.Warp(new Vector3(19, sheep6.nextPosition.y, -23));
                sheep7.Warp(new Vector3(0, sheep7.nextPosition.y, -12));
                sheep8.Warp(new Vector3(22, sheep8.nextPosition.y, -14));
                sheep9.Warp(new Vector3(15, sheep9.nextPosition.y, -5));
                sheep10.Warp(new Vector3(-2, sheep10.nextPosition.y, -38));
                sheep11.Warp(new Vector3(-16, sheep11.nextPosition.y, -25));
                sheep12.Warp(new Vector3(15, sheep12.nextPosition.y, -16));
                sheeps.SheepList.Add(sheep1.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep2.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep3.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep4.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep5.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep6.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep7.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep8.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep9.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep10.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep11.gameObject.GetComponent<SheepBehaviour>());
                sheeps.SheepList.Add(sheep12.gameObject.GetComponent<SheepBehaviour>());
                break;
            default:
                Debug.Log("Something broke on loading system :x");
                break;            
        }
        shepherd.GetComponent<ShepherdBehaviour>().NextWaypoint();
        areaManager.LoadCamera();
        areaManager.LoadArea();
    }
}
