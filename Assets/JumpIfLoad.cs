﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class JumpIfLoad : MonoBehaviour
{
    [SerializeField] GameObject game;
    void Start()
    {
        if (PlayerPrefs.GetInt("Waypoint") != 0)
        {
            game.SetActive(true);
            this.gameObject.SetActive(false);
        } else
        {
            this.GetComponent<PlayableDirector>().Play();
        }
    }
}
