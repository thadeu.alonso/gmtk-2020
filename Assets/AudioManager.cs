﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager AudioManagerInstance;
    [SerializeField] AudioSource _cinematicDoor;
    [SerializeField] AudioSource _openingDoor;
    [SerializeField] AudioSource _backGroundSong;
    [SerializeField] List<AudioClip> _dialogueClips;
    [SerializeField] AudioSource _dialogue;
    [SerializeField] List<AudioClip> _sheepSoundClips;
    [SerializeField] AudioSource _sheepSoundSource;
    [SerializeField] List<AudioClip> _commandsqqClips;
    [SerializeField] AudioSource _commandsqqSource;
    [SerializeField] List<AudioClip> _commandsqwClips;
    [SerializeField] AudioSource _commandsqwSource;
    [SerializeField] List<AudioClip> _commandswqClips;
    [SerializeField] AudioSource _commandswqSource;
    [SerializeField] List<AudioClip> _commandswwClips;
    [SerializeField] AudioSource _commandswwSource;
    [SerializeField] List<AudioClip> _dogWalkingClips;
    [SerializeField] AudioSource _dogWalkingSource;


    private void Start()
    {
        AudioManagerInstance = this;
    }

    public void PlayCinematicDoor()
    {
        _cinematicDoor.Play();
    }

    public void PlayOpeningDoor()
    {
        _openingDoor.Play();
    }


    public void PlaybackGroundSong()
    {
        _backGroundSong.Play();
    }

    public void PlayDialogueAudio()
    {
        int index = Random.Range(0, 25);
        _dialogue.clip = _dialogueClips[index];
        _dialogue.Play();
    }

    public void PlaySheepSound()
    {
        int index = Random.Range(0, 6);
        _sheepSoundSource.clip = _sheepSoundClips[index];
        _sheepSoundSource.Play();
    }

    public void Playcomandqq()
    {
        int index = Random.Range(0, 4);
        _commandsqqSource.clip = _commandsqqClips[index];
        _commandsqqSource.Play();
    }

    public void Playcomandqw()
    {
        int index = Random.Range(0, 4);
        _commandsqwSource.clip = _commandsqwClips[index];
        _commandsqwSource.Play();
    }

    public void Playcomandwq()
    {
        int index = Random.Range(0, 4);
        _commandswqSource.clip = _commandswqClips[index];
        _commandswqSource.Play();
    }

    public void Playcomandww()
    {
        int index = Random.Range(0, 4);
        _commandswwSource.clip = _commandswwClips[index];
        _commandswwSource.Play();
    }

    public void PlayDogWalking()
    {
        if (_dogWalkingSource.isPlaying == false)
        {
            int index = Random.Range(0, 4);
            _dogWalkingSource.clip = _dogWalkingClips[index];
            _dogWalkingSource.Play();
        }
    }
}
