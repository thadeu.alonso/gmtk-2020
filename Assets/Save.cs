﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save : MonoBehaviour
{
    [SerializeField] int waypoint;
 
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Shepherd")
        {
            PlayerPrefs.SetInt("Waypoint", waypoint);
        }
    }
}
