﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class DogBehaviour : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] float _moveDistance = 20f;
    [SerializeField] LayerMask _obstacleLayer;

    [SerializeField] ParticleSystem particles1;
    [SerializeField] ParticleSystem particles2;

    private NavMeshAgent _agent;
    private DialogueSystem _dialogueSystem;
    private Vector3 _destination;
    private float _forwardMove;
    private float _sideMove;

    private char[] _comboList;
    private int _totalKeys;
    private int _wCount;
    private int _qCount;
    private bool _stopped;
    private bool _anyKeyPressed;
    private float _timer;
    private float _cooldown = 0.5f;
    private float _originalDistance;
    private bool _startTimer;
    private NavMeshHit _closestEdgeHit;
    private bool _vestigialMovement;
    private float _remainginDistance = 0;
    private String _lastSide;
    public bool IsFreeToMove { get; set; }
    public bool gameBegun;

    public AudioClip qq;
    public AudioClip qw;
    public AudioClip wq;
    public AudioClip ww;
    private AudioSource audio;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponentInChildren<Animator>();
        _comboList = new char[2];
        _timer = _cooldown;
        _originalDistance = _moveDistance;
        audio = GetComponent<AudioSource>();
        IsFreeToMove = true;
        gameBegun = false;

        _dialogueSystem = FindObjectOfType<DialogueSystem>();
        _dialogueSystem.OnDialogueStart += OnDialogueStarts;
        _dialogueSystem.OnDialogueEnd += OnDialogueEnds;
    }

    private void OnDisable()
    {
        if(_dialogueSystem != null)
        {
            _dialogueSystem.OnDialogueStart -= OnDialogueStarts;
            _dialogueSystem.OnDialogueEnd -= OnDialogueEnds;
        }
    }

    private void OnDialogueStarts()
    {
        IsFreeToMove = false;
    }

    private void OnDialogueEnds()
    {
        IsFreeToMove = true;
    }

    private void Update()
    {
        /*if (!IsAgentMoving() && _vestigialMovement)
        {
            _vestigialMovement = false;
            if (_lastSide == "left")
                SetMoveDirection("left", _remainginDistance, false);
            else
                SetMoveDirection("right", _remainginDistance, false);
        }*/

        if (IsFreeToMove && gameBegun)
        {
            DetectKey(KeyCode.W, 'w');
            DetectKey(KeyCode.Q, 'q');

            if (_anyKeyPressed)
            {
                if (_timer <= 0f)
                {
                    _timer = _cooldown;
                    _anyKeyPressed = false;
                    _startTimer = false;
                    ResetCounters();
                }
                else
                {
                    if (!_startTimer)
                    {
                        _startTimer = true;
                    }

                    _timer -= Time.deltaTime;
                }
            }

            if (_totalKeys >= 2 && _startTimer)
            {
                string combo = "";

                for (int i = 0; i < 2; i++)
                {
                    combo += _comboList[i];
                    _comboList[i] = char.MinValue;
                }

                SetDirectionByCombo(combo);
                ResetCounters();
            }

            if (_forwardMove != 0f || _sideMove != 0f)
            {
                _agent.SetDestination(_destination);
            }
        }
    }

    private void LateUpdate()
    {
        bool isRunning = IsAgentMoving();
        _animator.SetBool("isRunning", isRunning);
    }

    private bool IsAgentMoving()
    {
        //Debug.Log(_agent.velocity.magnitude);
        /*if (_agent.velocity.magnitude > 1)
        {
            return true;
        }
        else
        {
            return false;
        }*/

        if (!_agent.pathPending)
        {
            if (_agent.remainingDistance <= _agent.stoppingDistance)
            {
                if (!_agent.hasPath || _agent.velocity.sqrMagnitude == 0f)
                {
                    // Done
                    return false;
                }
            }
        }

        return true;
    }

    public void MoveTo(Transform dogWaypoint)
    {
        _destination = transform.position;
        _forwardMove = 0f;
        _sideMove = 0f;
        ResetCounters();
        _agent.SetDestination(dogWaypoint.position);
    }

    private void ResetCounters()
    {
        _totalKeys = 0;
        _qCount = 0;
        _wCount = 0;
    }

    private void SetDirectionByCombo(string combo)
    {
        switch (combo)
        {
            case "ww":
                Debug.Log("Forward");
                _lastSide = "Forward";
                SetMoveDirection("forward", _moveDistance, true);
                AudioManager.AudioManagerInstance.Playcomandww();
                AudioManager.AudioManagerInstance.PlayDogWalking();
                break;
            case "qq":
                Debug.Log("Backwards");
                _lastSide = "Backwards";
                SetMoveDirection("backward", _moveDistance, true);
                AudioManager.AudioManagerInstance.Playcomandqq();
                AudioManager.AudioManagerInstance.PlayDogWalking();
                break;
            case "wq":
                Debug.Log("Left");
                _lastSide = "Left";
                SetMoveDirection("left", _moveDistance, true);
                AudioManager.AudioManagerInstance.Playcomandwq();
                AudioManager.AudioManagerInstance.PlayDogWalking();
                break;
            case "qw":
                Debug.Log("Right");
                _lastSide = "Right";
                SetMoveDirection("right", _moveDistance, true);
                AudioManager.AudioManagerInstance.Playcomandqw();
                AudioManager.AudioManagerInstance.PlayDogWalking();
                break;
        }
    }

    private void DetectKey(KeyCode keyCode, char keyChar)
    {
        if (Input.GetKeyDown(keyCode))
        {
            if (_totalKeys >= 2)
                return;

            if (!_anyKeyPressed)
                _anyKeyPressed = true;

            _comboList[_totalKeys] = keyChar;

            _totalKeys++;

            if (keyCode == KeyCode.Q)
            {
                _qCount++;
                particles1.Play();
            }
            if (keyCode == KeyCode.W)
            {
                _wCount++;
                particles2.Play();
            }
        }
    }

    private void SetMoveDirection(string direction, float _distance, bool _firstMovement)
    {
        RaycastHit hit;

        if (direction == "backward")
        {
            float realDistance = RayToChangeDistance(-transform.forward, _distance, _firstMovement);
            
            _forwardMove = -realDistance;
            _sideMove = 0f;
            _agent.isStopped = false;

            _destination = transform.position + transform.forward.normalized * _forwardMove;
        }

        if (direction == "forward")
        {
            float realDistance = RayToChangeDistance(transform.forward, _distance, _firstMovement);

            _forwardMove = realDistance;
            _sideMove = 0f;
            _agent.isStopped = false;
            _destination = transform.position + transform.forward.normalized * _forwardMove;
        }

        if (direction == "right")
        {
            float realDistance = RayToChangeDistance(transform.right, _distance, _firstMovement);

            _forwardMove = 0f;
            _sideMove = realDistance;
            _agent.isStopped = false;
            _destination = transform.position + transform.right.normalized * _sideMove;
        }

        if (direction == "left")
        {
            float realDistance = RayToChangeDistance(-transform.right, _distance, _firstMovement);

            _forwardMove = 0f;
            _sideMove = -realDistance;
            _agent.isStopped = false;
            _destination = transform.position + transform.right.normalized * _sideMove;
        }
    }

    public float RayToChangeDistance(Vector3 direction, float _distance, bool _firstMovement)
    {
        RaycastHit hit;
        Vector3 positionToCheck = transform.position + direction * _distance;

        if (Physics.SphereCast(transform.position, 1f, direction, out hit, _originalDistance, _obstacleLayer))
        {
            if (_firstMovement)
            {
                _remainginDistance = 3;
            }
            else
            {
                _remainginDistance = _distance - hit.distance;
            }
            _vestigialMovement = true;
            return hit.distance - 1f;
        }
        else
        {
            var path = new NavMeshPath();
            _agent.CalculatePath(positionToCheck, path);

            if (path.status == NavMeshPathStatus.PathPartial || path.status == NavMeshPathStatus.PathInvalid)
            {
                if(NavMesh.SamplePosition(positionToCheck, out _closestEdgeHit, _originalDistance, NavMesh.AllAreas))
                {
                    if (_firstMovement)
                    {
                        _remainginDistance = 3;
                    }
                    else
                    {
                        _remainginDistance = _distance - hit.distance;
                    }
                    _vestigialMovement = true;

                    return _distance - _closestEdgeHit.distance;
                }
            }
            else
            {
                return _distance;
            }
        }
        return 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * _moveDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * -_moveDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.right * _moveDistance);
                                                     
        Gizmos.color = Color.red;                    
        Gizmos.DrawRay(transform.position, transform.right * -_moveDistance);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_destination, 1f);

        if(_closestEdgeHit.position != Vector3.zero)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(_closestEdgeHit.position, 1f);
        }
    }
}
