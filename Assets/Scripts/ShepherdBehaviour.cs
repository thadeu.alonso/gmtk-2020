﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShepherdBehaviour : MonoBehaviour
{
    public int _currentWaypoint;
    [SerializeField] List<Transform> waypoints;

    private NavMeshAgent _agent;
    private DialogueSystem dialogueSystem;

    public Action<Transform> OnReachWaypoint;
    private bool _isGoingToNextArea;

    private void Awake()
    {
        dialogueSystem = FindObjectOfType<DialogueSystem>();

        dialogueSystem.OnMoveDialogueEnds += NextWaypoint;
        _agent = GetComponent<NavMeshAgent>();
        _agent.SetDestination(waypoints[_currentWaypoint].position);
        _agent.isStopped = true;
    }

    private void Update()
    {
        if (!_isGoingToNextArea)
            return;

        if (_agent.hasPath && _agent.remainingDistance <= _agent.stoppingDistance)
        {
            OnReachWaypoint?.Invoke(waypoints[_currentWaypoint]);
            _isGoingToNextArea = false;
        }
    }

    public void NextWaypoint()
    {
        if (_currentWaypoint < waypoints.Count - 1)
        {
            _currentWaypoint++;
            _agent.SetDestination(waypoints[_currentWaypoint].position);
        }
    }

    private void OnDisable()
    {
        if (dialogueSystem != null)
            dialogueSystem.OnMoveDialogueEnds -= NextWaypoint;
    }

    public void MoveToNextArea()
    {
        NextWaypoint();
        _isGoingToNextArea = true;
    }
}
