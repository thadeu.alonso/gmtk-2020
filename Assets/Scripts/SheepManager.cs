﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SheepManager : MonoBehaviour
{
    public List<SheepBehaviour> SheepList;

    public void SaveSheep(SheepBehaviour sheep)
    {
        sheep.IsSafe = true;
    }

    public bool IsAllHerdSafe()
    {
        bool isAllSafe = true;

        foreach (var sheep in SheepList)
        {
            if (!sheep.IsSafe)
            {
                isAllSafe = false;
                break;
            }
        }

        return isAllSafe;
    }

    public void ResetHerdState()
    {
        foreach (var sheep in SheepList)
        {
            sheep.IsSafe = false;
        }
    }

    public void AddNewSheeps(List<SheepBehaviour> sheepsToAddOnHerd)
    {
        SheepList.AddRange(sheepsToAddOnHerd);
    }
}
