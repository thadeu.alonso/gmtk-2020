﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class DialogueSystem : MonoBehaviour
{

    public Text dialogueText;

    public GameObject dialogueGUI;
    public Transform dialogueBoxGUI;

    public float[] letterDelay;
    public float letterMultiplier = 0.5f;

    public KeyCode DialogueInput = KeyCode.Space;

    public string[] dialogueLines;

    public bool letterIsMultiplied = false;
    public bool dialogueActive = false;
    public bool dialogueEnded = false;
    private bool shouldMoveAtEnd;
    public bool outOfRange = true;

    public AudioClip audioClip;

    public Action OnDialogueStart;
    public Action OnDialogueEnd;
    public Action OnMoveDialogueEnds;

    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        dialogueText.text = "";
    }

    public void EnterRangeOfNPC(bool moveAtEnd)
    {
        shouldMoveAtEnd = moveAtEnd;
        outOfRange = false;

        dialogueBoxGUI.gameObject.SetActive(true);
        if(!dialogueActive)
        {
            dialogueActive = true;
            OnDialogueStart?.Invoke();
            StartCoroutine(StartDialogue());
        }
    }

    private IEnumerator StartDialogue()
    {
        if(outOfRange == false)
        {
            int dialogueLength = dialogueLines.Length;
            int currentDialogueIndex = 0;

            while (currentDialogueIndex < dialogueLength || !letterIsMultiplied)
            {
                if(!letterIsMultiplied)
                {
                    letterIsMultiplied = true;
                    StartCoroutine(DisplayString(letterDelay[currentDialogueIndex], dialogueLines[currentDialogueIndex++]));
                    
                    if(currentDialogueIndex >= dialogueLength)
                    {
                        dialogueEnded = true;
                    }
                    
                }
                yield return 0;
            }

            while (true)
            {
                if(Input.GetKeyDown(DialogueInput) && dialogueEnded == false)
                {
                    if (shouldMoveAtEnd)
                        OnMoveDialogueEnds?.Invoke();

                    break;
                }
                yield return 0;
            }

            dialogueEnded = false;
            dialogueActive = false;
            DropDialogue();
            OnDialogueEnd?.Invoke();
        }
    }

    private IEnumerator DisplayString(float letterDelay, string stringToDisplay)
    {
        if(outOfRange == false)
        {
            int stringLength = stringToDisplay.Length;
            int currentCharacterIndex = 0;

            dialogueText.text = "";

            while (currentCharacterIndex < stringLength)
            {
                dialogueText.text += stringToDisplay[currentCharacterIndex];
                currentCharacterIndex++;

                if(currentCharacterIndex < stringLength)
                {
                    if(Input.GetKey(DialogueInput))
                    {
                        yield return new WaitForSeconds(letterMultiplier);
                        AudioManager.AudioManagerInstance.PlayDialogueAudio();
                    }
                    else
                    {
                        yield return new WaitForSeconds(letterDelay);
                        AudioManager.AudioManagerInstance.PlayDialogueAudio();
                    }
                }
                else
                {
                    dialogueEnded = false;
                    break;
                }
            }

            while (true)
            {
                if(Input.GetKeyDown(DialogueInput))
                {
                    break;
                }
                yield return 0;
            }
            dialogueEnded = false;
            letterIsMultiplied = false;
            dialogueText.text = "";
        }
    }

    public void DropDialogue()
    {
        dialogueGUI.SetActive(false);
        dialogueBoxGUI.gameObject.SetActive(false);
    }
    public void OutOfRange()
    {
        outOfRange = true;
        if(outOfRange == true)
        {
            letterIsMultiplied = false;
            dialogueActive = false;
            StopAllCoroutines();
            dialogueGUI.SetActive(false);
            dialogueBoxGUI.gameObject.SetActive(false);
        }
    }
}
