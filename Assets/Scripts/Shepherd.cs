﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.AI;

public class Shepherd : MonoBehaviour
{
    protected float angle = 0.0f;

    // Velocidade angular do cachorro. Quanto maior, mais rápido ele fica rodando ao redor das ovelhas.
    public float angleSpeed = 3.0f;

    // Velocidade linear do cachorro. Quanto maior, mais rápido ele vai para onde deveria estar.
    // Esse valor deveria ser próximo da velocidade angular acima. Muito abaixo e o cachorro nunca vai alcançar
    // o target. Muito maior, vai dar uns overshootings estranhos e uns tremeliques.s
    public float speed = 3.0f;

    // Margem de segurança, para evitar que o cachorro passe "por dentro" do grupo de ovelhas.
    // Bônus percentual em cima do raio total das ovelhas (0.5 = 50% de margem de segurança).
    public float margin = 0.5f;

    // Número de ovelhas para a amostragem de ovelhas mais próximas. Números mais altos vão produzir um movimento
    // mais arredondado, porém mais afastado também.
    public int N = 3;

    // Ovelhas as quais serão arrebanhadas.
    public List<GameObject> allTheSheep;
    private NavMeshAgent _agent;
    public Animator _animator;

    // Usado para fazer os desenhos de debug das linhas e aumentar a escala das ovelhas mais próximas
    public bool debug = false;

    private bool _clockwise;
    private bool _isStopped;
    public Area _currentArea;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        allTheSheep = new List<GameObject>();
        EnterArea();
    }

    private void EnterArea()
    {
        foreach (var sheep in _currentArea.SheepsInArea)
        {
            allTheSheep.Add(sheep);
        }
    }

    void Update()
    {
        // Pegando a posição antiga, para poder traçar o deslocamento
        Vector3 oldPos = transform.position;

        // Essa é a nova posição, alvo do nosso cãozinho.
        Vector3 newPos;

        if (Input.GetKeyDown(KeyCode.W))
        {
            _isStopped = !_isStopped;
        }

        bool reachDestination = _agent.remainingDistance > _agent.stoppingDistance;
        _animator.SetBool("IsRunning", reachDestination);

        if (_isStopped)
            return;

        if (allTheSheep.Count <= 0)
            return;

        if (_clockwise)
        {
            // Começamos aumentando o ângulo dele, que vai ser usado para girar ao redor do centro
            // das ovelhas
            angle += angleSpeed * Time.deltaTime;
        }
        else
        {
            angle -= angleSpeed * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.D))
            _clockwise = true;

        if (Input.GetKeyDown(KeyCode.A))
            _clockwise = false;

        // Por falar nele: vamos calcular o centro das ovelhas
        Vector3 center = Vector3.zero;
        foreach(GameObject sheep in allTheSheep)
        {
            center += sheep.transform.position / allTheSheep.Count;
        }

        // Agora que temos o centro, vamos calcular o raio máximo (a distância da ovelha
        // mais distante ao centro calculado ali em cima)
        float radius = 0.0f;
        foreach(GameObject sheep in allTheSheep)
        {
            if(debug)
            {
                Debug.DrawLine(center, sheep.transform.position, Color.yellow);
            }
            radius = Mathf.Max(radius, (sheep.transform.position-center).magnitude);
        }

        // Uma vez calculado o raio, vamos assumir que a nova posição do cachorrinho seria
        // uma posição básica: em esseência, girando ao redor das ovelhas no raio máximo, mais uma gordura.
        newPos = center + new Vector3(Mathf.Cos(angle), 0.0f, Mathf.Sin(angle)) * radius * (1.0f + margin);

        // Agora vamos melhorar essa nova posição. Vamos aproximá-la do centro das N ovelhinhas
        // mais próximas. 
        List<SheepOrder> nearest = new List<SheepOrder>();
        foreach(GameObject sheep in allTheSheep)
        {
            float dist = (newPos - sheep.transform.position).magnitude;
            nearest.Add(new SheepOrder(sheep, dist));
        }
        nearest.Sort(new SheepComparer());
        nearest.RemoveRange(N, nearest.Count - N);
        // Pronto! Com isso, temos apenas as N ovelhas mais próximas da nova posição (onde nosso cachorro vai mirar).

        // Agora vamos calcular a posição média dessas N ovelhas:
        Vector3 nearestCenter = Vector3.zero;
        float w = 0.0f;
        foreach(SheepOrder order in nearest)
        {
            if(debug)
            {
                Debug.DrawLine(newPos, order.sheep.transform.position, Color.magenta);
            }
            nearestCenter += order.sheep.transform.position / nearest.Count;

            // Esse W aqui existe para que possamos atribuir um "peso". Em essência, uma ovelha muito desgarrada
            // pode influencar o quanto pegamos desse centro das N mais próximas. Queremos que quando uma ovelha
            // esteja muito longe das outras (ou seja, que a sua distância do centro seja próxima do raio máximo)
            // seu peso seja maior na conta.
            w = Mathf.Max(w, (order.sheep.transform.position-center).magnitude / radius);
        }

        if(debug)
        {
            Debug.DrawLine(newPos, transform.position, Color.blue);
        }

        // Agora é só aplicar a média ponderada, usando o peso calculado ali em cima.
        newPos = newPos * w + nearestCenter * (1.0f-w);

        // Por fim, fazemos com que o cãozinho corra atrás de sua nova posição, usando o vetor direção
        // newPos - transform.position
        //transform.position += (newPos - transform.position) * Time.deltaTime * speed;
        //_agent.speed = speed * 40f;
        //_agent.angularSpeed = angleSpeed * 200f;
        _agent.SetDestination(newPos);

        // Isso aqui desenha a trajetória dele
        if (debug)
        {
            Debug.DrawLine(oldPos, transform.position, Color.green, 1.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Area")
            return;

        var area = other.GetComponent<Area>();
        
        if (area == _currentArea)
            return;

        _currentArea = area;

        EnterArea();
    }
}

// Classes e Structs complementares para ajudar na ordenação
public struct SheepOrder 
{
    public GameObject sheep;

    public float distanceFromCenter;

    public SheepOrder(GameObject sheep, float distance)
    {
        this.sheep = sheep;
        this.distanceFromCenter = distance;
    }
}

public class SheepComparer : IComparer<SheepOrder>
{
    public int Compare(SheepOrder a, SheepOrder b)
    {
        return a.distanceFromCenter > b.distanceFromCenter? 1 : -1;
    }

}