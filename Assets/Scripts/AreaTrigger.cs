﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour
{
    [SerializeField] CinemachineFreeLook _areaCamera;
    [SerializeField] List<SheepBehaviour> _sheepsToAddOnHerd;
    [SerializeField] Transform _shepherdWaypoint;
    [SerializeField] Transform _sheepWaypoint;
    [SerializeField] Transform _dogWaypoint;
    [SerializeField] Transform _gateTransform;
    [SerializeField] float _gateRotationDegrees = 90f;
    [SerializeField] int _gateRotationDuration = 1;

    private ClickToMove _moveScript;
    private DogBehaviour _dog;
    private ShepherdBehaviour _shepherd;
    private bool _isActive;
    private SheepManager _sheepManager;
    private AreaManager _areaManager;

    public void Init(AreaManager areaManager)
    {
        _areaManager = areaManager;
        _moveScript = FindObjectOfType<ClickToMove>();
        _dog = FindObjectOfType<DogBehaviour>();
        _shepherd = FindObjectOfType<ShepherdBehaviour>();
        _sheepManager = FindObjectOfType<SheepManager>();
    
        _shepherd.OnReachWaypoint += OnReachWaypoint;

        OpenArea();
    }

    private IEnumerator Rotate(Vector3 angles, int duration, Action onFinish)
    {
        AudioManager.AudioManagerInstance.PlayOpeningDoor();
        Quaternion startRotation = _gateTransform.rotation;
        Quaternion endRotation = Quaternion.Euler(angles) * startRotation;

        for (float t = 0; t < duration; t += Time.deltaTime)
        {
            _gateTransform.rotation = Quaternion.Lerp(startRotation, endRotation, t / duration);
            yield return null;
        }

        _gateTransform.rotation = endRotation;
        onFinish();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_isActive)
            return;

        if(other.gameObject.tag == "Shepherd")
        {
            UpdateCamera();
            _dog.IsFreeToMove = true;
        }
        else if(other.gameObject.tag == "Sheep")
        {
            AudioManager.AudioManagerInstance.PlaySheepSound();
            var sheepBehaviour = other.gameObject.GetComponent<SheepBehaviour>();

            if (!sheepBehaviour.IsSafe)
            {
                _sheepManager.SaveSheep(sheepBehaviour);
                sheepBehaviour.MoveTo(_sheepWaypoint);

                if(_sheepManager.IsAllHerdSafe())
                {
                    _dog.IsFreeToMove = false;
                    _dog.MoveTo(_dogWaypoint);
                    _shepherd.MoveToNextArea();
                    _areaManager.CurrentArea = null;
                    _sheepManager.AddNewSheeps(_sheepsToAddOnHerd);
                    _sheepManager.ResetHerdState();
                }
            }
        }
    }

    public void UpdateCamera()
    {
        _moveScript = FindObjectOfType<ClickToMove>();
        _moveScript.ChangeCamera(_areaCamera);
    }

    private void OnReachWaypoint(Transform waypoint)
    {
        if (waypoint == _shepherdWaypoint && _isActive)
        {
            CloseArea();
            _areaManager.EndCurrentArea();
        }
    }

    public void OpenArea()
    {
        StartCoroutine(Rotate(new Vector3(0, _gateRotationDegrees, 0), _gateRotationDuration, () => { _isActive = true; }));
    }

    public void CloseArea()
    {
        StartCoroutine(Rotate(new Vector3(0, -_gateRotationDegrees, 0), _gateRotationDuration, () => { _isActive = false; }));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = _isActive ? Color.blue : Color.red;
        Gizmos.DrawSphere(transform.position + Vector3.up * 3f, .8f);
    }
}
