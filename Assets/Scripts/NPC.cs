﻿using System;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class NPC : MonoBehaviour
{
    public bool moveAtEnd;
    public Transform ChatBackground;
    public Transform NPCCharacter;

    private DialogueSystem dialogueSystem;

    [TextArea(5, 10)]
    public string[] sentences;
    public float[] sentencesSpeed;

    [SerializeField] float xOffset;
    [SerializeField] float yOffset;

    [SerializeField] UnityEvent OnDialogueEndEvent;

    private NPC npcScript;

    void Start()
    {
        dialogueSystem = FindObjectOfType<DialogueSystem>();
        dialogueSystem.OnMoveDialogueEnds += OnDialogueEnd;
    }
    private void OnDisable()
    {
        dialogueSystem.OnMoveDialogueEnds -= OnDialogueEnd;
    }

    private void OnDialogueEnd()
    {
        if(dialogueSystem.dialogueLines == sentences)
        {
            OnDialogueEndEvent?.Invoke();
        }
    }
    
    void Update()
    {
        Vector3 worldPosition = new Vector3(NPCCharacter.position.x + ChatOrganizer.chatOrganizerinstance.xOffset, NPCCharacter.position.y + ChatOrganizer.chatOrganizerinstance.yOffset, NPCCharacter.position.z);
        Vector3 Pos = Camera.main.WorldToScreenPoint(worldPosition);
        ChatBackground.position = Pos;
    }

    public void OnTriggerEnter(Collider other)
    {
        npcScript = GetComponent<NPC>();
        npcScript.enabled = true;

        if(other.gameObject.tag == "Shepherd")
        {
            ChatOrganizer.chatOrganizerinstance.xOffset = xOffset;
            ChatOrganizer.chatOrganizerinstance.yOffset = yOffset;

            npcScript.enabled = true;
            dialogueSystem.dialogueLines = sentences;
            dialogueSystem.letterDelay = sentencesSpeed;
            dialogueSystem.EnterRangeOfNPC(moveAtEnd);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        npcScript.enabled = false;
    }
}
