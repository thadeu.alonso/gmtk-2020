﻿using Cinemachine;
using UnityEngine;

public class ClickToMove : MonoBehaviour
{
    [SerializeField] CinemachineFreeLook _currentCamera;

    [Header("Debug")]
    [SerializeField] CinemachineFreeLook _startCamera;

    private void Awake()
    {
        if (_startCamera != null)
            ChangeCamera(_startCamera);
    }
    public void ChangeCamera(CinemachineFreeLook newCamera)
    {
        _currentCamera.Priority = 0;
        newCamera.Priority = 1;
    }
}