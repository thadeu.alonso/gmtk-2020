﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SheepBehaviour : MonoBehaviour
{
    [SerializeField] bool _debug;

    [Header("Components")]
    [SerializeField] Animator _sheepAnimator;

    [Header("Movement Settings")]
    [SerializeField] LayerMask _obstacleLayer;
    [SerializeField] float _moveSpeed;
    [SerializeField] float _runSpeed;
    [SerializeField] float _runDistance = 10f;
    [SerializeField] float _rangeDetection;
    [SerializeField] float _scaredCooldown;
    [SerializeField] float _minTimeToMove;
    [SerializeField] float _maxTimeToMove;
    [SerializeField] float _minRangeX;
    [SerializeField] float _maxRangeX;
    [SerializeField] float _minRangeZ;
    [SerializeField] float _maxRangeZ;

    private NavMeshAgent _agent;
    private Transform _playerTransform;
    private float _timer;
    private bool _isScared;
    private float _scaredTimer;
    private Vector3 _playerDirection;
    private bool _isFreeToMove;
    private NavMeshHit _closestEdgeHit;

    public bool IsSafe { get; set; }

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _timer = UnityEngine.Random.Range(_minTimeToMove, _maxTimeToMove);
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        _isFreeToMove = true;
    }

    private void Update()
    {
        if (_isFreeToMove)
        {
            _playerDirection = _playerTransform.position - transform.position;
            _playerDirection.Normalize();
            _playerDirection *= -1f;

            Scare();

            if (_isScared)
            {
                if (_scaredTimer <= 0f)
                {
                    _isScared = false;
                    _agent.speed = _moveSpeed;
                    _scaredTimer = _scaredCooldown;
                }
                else
                {
                    _scaredTimer -= Time.deltaTime;
                }
            }
            else
            {
                if (_timer <= 0f)
                {
                    float randomX = UnityEngine.Random.Range(transform.position.x + _minRangeX, transform.transform.position.x + _maxRangeX);
                    float randomZ = UnityEngine.Random.Range(transform.position.z + _minRangeZ, transform.transform.position.z + _maxRangeZ);
                    Vector3 randomDestination = new Vector3(randomX, 0f, randomZ);

                    CalculateDestination(randomDestination);

                    _timer = UnityEngine.Random.Range(_minTimeToMove, _maxTimeToMove);
                }
                else
                {
                    _timer -= Time.deltaTime;
                }
            }
        }

        bool reachDestination = Vector3.Distance(transform.position, _agent.destination) <= _agent.stoppingDistance;

        if (!_isFreeToMove && reachDestination)
            _isFreeToMove = true;
        
        _sheepAnimator.speed = CalculateAnimationSpeed();
        if (_agent.velocity.magnitude == 0)
        {
            _sheepAnimator.SetBool("IsWalking", false);
            _sheepAnimator.SetBool("IsRunning", false);
        } else
        if (_agent.velocity.magnitude <= 6)
        {
            _sheepAnimator.SetBool("IsWalking", true);
            _sheepAnimator.SetBool("IsRunning", false);
        } else
        {
            _sheepAnimator.SetBool("IsWalking", false);
            _sheepAnimator.SetBool("IsRunning", true);
        }
    }

    float CalculateAnimationSpeed()
    {
        float speed;
        speed = (3 * _agent.velocity.magnitude) / _agent.speed;
        return speed;
    }

    private void LateUpdate()
    {
        if (_agent.velocity.magnitude == 0)
        {
            _sheepAnimator.SetBool("IsWalking", false);
            _sheepAnimator.SetBool("IsRunning", false);
        } else
        if (_agent.velocity.magnitude <= 6)
        {
            _sheepAnimator.speed = CalculateAnimationSpeed();
            _sheepAnimator.SetBool("IsWalking", true);
            _sheepAnimator.SetBool("IsRunning", false);
        } else
        {
            _sheepAnimator.speed = CalculateAnimationSpeed();
            _sheepAnimator.SetBool("IsWalking", false);
            _sheepAnimator.SetBool("IsRunning", true);
        }
    }

    public void MoveTo(Transform destination)
    {
        _agent.SetDestination(new Vector3(destination.position.x + UnityEngine.Random.Range(-7.5f, 7.5f), destination.position.y, destination.position.z + UnityEngine.Random.Range(-7.5f, 7.5f)));
        _isFreeToMove = false;
    }

    public void Scare()
    {
        if(Vector3.Distance(transform.position, _playerTransform.position) <= _rangeDetection)
        {
            _agent.speed = _runSpeed;
            var destination = transform.position + _playerDirection * _runDistance;
            CalculateDestination(destination);
            _isScared = true;
        }
    }

    private void CalculateDestination(Vector3 destination)
    {
        RaycastHit hit;
        bool isBlocked = Physics.SphereCast(transform.position, 1f, destination, out hit, _runDistance, _obstacleLayer);

        Vector3 direction = destination - transform.position;
        Color rayColor = isBlocked ? Color.red : Color.green;
        Debug.DrawRay(transform.position, direction.normalized * _runDistance, rayColor, .5f);
        Debug.DrawRay(transform.position, Vector3.Cross(destination, Vector3.up).normalized * _runDistance, Color.yellow, .5f);

        if (isBlocked)
        {
            AreaTrigger areaTrigger = hit.transform.gameObject.GetComponent<AreaTrigger>();

            if (areaTrigger != null)
            {
                var areaManager = FindObjectOfType<AreaManager>();

                if (areaManager.CurrentArea != areaTrigger)
                {
                    destination = transform.position + Vector3.Cross(destination, Vector3.up).normalized * _runDistance;
                }
                if (!_isScared)
                {
                    destination = transform.position - Vector3.Cross(destination, Vector3.up).normalized * _runDistance;
                }
            }
            else
            {
                if( _isScared)
                {
                    var path = new NavMeshPath();
                    _agent.CalculatePath(destination, path);
                    if (path.status == NavMeshPathStatus.PathPartial || path.status == NavMeshPathStatus.PathInvalid)
                    {
                        if (NavMesh.SamplePosition(destination, out _closestEdgeHit, 50, NavMesh.AllAreas))
                        {

                            destination = _closestEdgeHit.position;
                        }
                    }
                }
                if (!_isScared)
                {
                    destination = transform.position - Vector3.Cross(destination, Vector3.up).normalized * _runDistance;
                }
            }
        }        
        _agent.SetDestination(destination);
    }

    private void OnDrawGizmos()
    {
        if (_playerTransform != null)
        {
            var distance = Vector3.Distance(transform.position, _playerTransform.position);

            //Gizmos.color = distance <= _rangeDetection ? Color.green : Color.red;
            //Gizmos.DrawRay(transform.position, _playerDirection * _runDistance);

            if (_agent.destination != null)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(_agent.destination, 1f);
            }
        }

        if (_debug)
        {
            Gizmos.color = IsSafe ? Color.green : Color.red;
            Gizmos.DrawWireSphere(transform.position, _rangeDetection);
        }
    }
}
