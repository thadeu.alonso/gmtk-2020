﻿using System.Collections.Generic;
using UnityEngine;

public class AreaManager : MonoBehaviour
{
    public int _currentIndex;
    [SerializeField] List<AreaTrigger> _areaTriggers;
    [SerializeField] GameObject EndCinematic;

    public AreaTrigger CurrentArea;

    public void ActivateCurrentArea()
    {
        if (_currentIndex < _areaTriggers.Count)
        {
            CurrentArea = _areaTriggers[_currentIndex];
            CurrentArea.Init(this);
        }
    }

    public void LoadArea()
    {
        CurrentArea = _areaTriggers[_currentIndex];
        EndCurrentArea();
    }

    public void LoadCamera()
    {
        CurrentArea = _areaTriggers[_currentIndex];
        CurrentArea.UpdateCamera();
    }

    public void EndCurrentArea()
    {
        _currentIndex++;

        if (_currentIndex < _areaTriggers.Count)
        {
            ActivateCurrentArea();
        }
        /*else
        {
            EndCinematic.SetActive(true);
        }*/
    }
}
