﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StartGame : MonoBehaviour
{
    [SerializeField] NavMeshAgent _agent;
    [SerializeField] GameObject menu;
    [SerializeField] DogBehaviour dog;
    [SerializeField] AreaManager areaManager;

    public void OnClick()
    {
        if(_agent.GetComponent<ShepherdBehaviour>()._currentWaypoint == 0)
        {
            areaManager.ActivateCurrentArea();
        }
        _agent.isStopped = false;
        Destroy(menu);
        dog.gameBegun = true;
    }
}
