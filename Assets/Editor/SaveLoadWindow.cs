﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveLoadWindow : EditorWindow
{
    [SerializeField] int savedIndex;
    [MenuItem ("Tools/Save Manager")]
    public static void  ShowWindow () {
        SaveLoadWindow window = (SaveLoadWindow)EditorWindow.GetWindow(typeof(SaveLoadWindow));
    }
    
    void OnGUI () {
        savedIndex = EditorGUILayout.IntField ("Saved Area", savedIndex);

        if (GUILayout.Button("Save Game"))
        {
            PlayerPrefs.SetInt("Waypoint", savedIndex);
        }
        if (GUILayout.Button("Delete Save"))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
